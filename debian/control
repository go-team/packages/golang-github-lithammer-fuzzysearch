Source: golang-github-lithammer-fuzzysearch
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Takuma Shibuya <shibuuuu5@gmail.com>
Section: golang
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-golang-x-text-dev
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-lithammer-fuzzysearch
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-lithammer-fuzzysearch.git
Homepage: https://github.com/lithammer/fuzzysearch
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/lithammer/fuzzysearch

Package: golang-github-lithammer-fuzzysearch-dev
Architecture: all
Depends: golang-golang-x-text-dev,
         ${misc:Depends}
Multi-Arch: foreign
Description: FuzzySearch provides tiny and fast fuzzy search
 Fuzzy searching allows for flexibly matching a string with partial input,
 useful for filtering data very quickly based on lightweight user input
 .
 Inspired by bevacqua/fuzzysearch
 (https://github.com/bevacqua/fuzzysearch), a fuzzy
 matching library written in JavaScript. But contains
 some extras like ranking using Levenshtein distance
 (http://en.wikipedia.org/wiki/Levenshtein_distance) (see RankMatch()
 (https://godoc.org/github.com/lithammer/fuzzysearch/fuzzy#RankMatch))
 and finding matches in a list of words (see Find()
 (https://godoc.org/github.com/lithammer/fuzzysearch/fuzzy#Find)).
 .
 The current implementation uses the algorithm suggested by Mr. Aleph,
 a Russian compiler engineer working at V8
